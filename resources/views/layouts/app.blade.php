<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="description" content="Easily keep track and visualize the value of your entire cryptocurrency portfolio in real time with just a few clicks." />
    <meta property="og:title" content="My Coin Tracker" />
    <meta property="og:image" content="{{ asset('img/social-image.jpg') }}" />
    <meta property="og:url" content="{{ URL::to('/') }}" />
    <meta property="og:description" content="Easily keep track and visualize the value of your entire cryptocurrency portfolio in real time with just a few clicks." />
    <meta name="google-site-verification" content="H2APYYmFAo-8oPN7BvQvu507Xlb_NtSV5OOc9VKuzpE" />

    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('img/favicons/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('img/favicons/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('img/favicons/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('img/favicons/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('img/favicons/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('img/favicons/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('img/favicons/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('img/favicons/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('img/favicons/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="196x196"  href="{{ asset('img/favicons/favicon-196x196.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('img/favicons/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('img/favicons/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('img/favicons/favicon-16x16.png') }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ asset('img/favicons/ms-icon-144x144.png') }}">
    <meta name="theme-color" content="#ffffff">

    <title>{{ config('app.name', 'My Coin Tracker') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
      (adsbygoogle = window.adsbygoogle || []).push({
        google_ad_client: "ca-pub-4370802149187412",
        enable_page_level_ads: true
      });
    </script>
</head>
<body>
    <div id="app">
        <header class="o-header">
            <div class="grid-container">
                <div class="grid-x align-middle">
                    <div class="cell medium-6 small-12">
                        <label for="menuToggle" aria-label="Menu Toggle" class="a-menuToggle hide-for-medium">
                            <input type="checkbox" id="menuToggle" name="menuToggle">
                            @svg('menu')
                            <nav class="a-toggleNav">
                                @if(!Auth::check())
                                <a href="{{ route('login') }}">login</a>
                                <a href="{{ route('register') }}">register</a>
                                @else
                                <span class="a-toggleNav__user">Welcome {{ Auth::user()->name }}</span>
                                <a href="{{ route('logout') }}">logout</a>
                                @endif
                            </nav>
                        </label>
                        <!-- <a href="{{ route('home') }}" class="a-logo">mycointracker</a> -->
                        <a href="{{ route('home') }}" class="a-logo" alt="My Coin Tracker Home">
                            @svg('logo')
                        </a>
                    </div>
                    <div class="cell small-6 text-right hide-for-small-only">
                        @if(!Auth::check())
                        <a href="{{ route('register') }}" class="button">sign up</a>
                        <a href="{{ route('login') }}" class="button">login</a>
                        @else
                        <span class="text-right">Welcome, {{ Auth::user()->name }}</span>
                        |
                        <a href="{{ route('logout') }}">Logout</a>
                        @endif
                    </div>
                </div>
            </div>
        </header>
        <main class="o-mainContent">
            <div class="grid-container">
                @yield('content')
            </div>
        </main>
        <footer class="o-footer">
            <div class="grid-container">
                <div class="grid-x align-middle grid-margin-y">
                    <div class="cell small-12 medium-6">
                        &copy; mycointracker {{ date('Y') }}. all rights reserved.
                    </div>
                    <div class="small-12 cell medium-6 text-right">
                        <nav class="m-footerNav">
                            <a href="{{ route('news') }}" alt="Crypto News">news</a>
                            <a href="#">contact</a>
                            <a href="{{ route('privacy') }}" alt="My Coin Tracker | Privacy Policy">privacy</a>
                            <a href="{{ route('terms') }}" alt="My Coin Tracker | Terms of Service">terms</a>
                        </nav>
                    </div>
                </div>
                <div class="grid-x m-footerBottom align-middle">
                    <div class="small-12 cell medium-6">
                        <p><strong>Donate BTC: </strong>1E54GXnbZLqqH2Da9uQcmrZMZpjkaxnMQm</p>
                        <p><strong>Donate ETH: </strong>0x3D59996D8439e5954A1987a0e38F8ce2B6A3EE11</p>
                    </div>
                    <div class="small-12 cell medium-6 text-right">
                        <nav class="m-socialNav">
                            <a href="https://www.facebook.com/mycointracker/" target="_blank" alt="My Coin Tracker | Facebook">@svg('facebook')</a>
                            <a href="https://twitter.com/mycointracker" target="_blank" alt="My Coin Tracker | Twitter">@svg('twitter')</a>
                        </nav>
                    </div>
                </div>
            </div>
        </footer>
    </div>
<!--     <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>]
    <ins class="adsbygoogle"
         style="display:block"
         data-ad-client="ca-pub-4370802149187412"
         data-ad-slot="5076440124"
         data-ad-format="auto"></ins>
    <script>
    (adsbygoogle = window.adsbygoogle || []).push({});
    </script> -->
    <!-- Scripts -->
    <script>
        window.laravel = {!! json_encode([ 'loggedIn' => Auth::check() ]) !!}
    </script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-111670895-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-111670895-1');
    </script>
</body>
</html>
