@extends('layouts.app')

@section('content')
<div class="grid-x grid-margin-y">
    <div class="cell">
        <div class="o-card">
            <header class="o-card__header">
                <div class="grid-x align-middle">
                    <div class="cell small-12 medium-6">
                        <h2>login</h2>
                    </div>
                </div>
            </header>
            <div class="o-card__content">
                <form method="POST" action="{{ route('login') }}">
                    <div class="cell small-12">
                        {{ csrf_field() }}
                        <div class="m-formItem">
                            @if ($errors->has('email'))
                                <span class="label alert">{{ $errors->first('email') }}</span>
                            @endif
                            <input 
                                type="text" 
                                name="email"
                                placeholder="Email:"
                                autocomplete="off">
                        </div>
                        <div class="m-formItem">
                            @if ($errors->has('password'))
                                <span class="label alert">{{ $errors->first('password') }}</span>
                            @endif
                            <input 
                                type="password" 
                                name="password"
                                placeholder="Password:"
                                autocomplete="off">
                        </div>
                    </div>
                    <div class="cell small-12 text-center align-center" style="margin-top: 20px;">
                        <button 
                            class="button">login</button>
                    </div>

                    <a style="margin-top: 20px" href="{{ route('password.request') }}">
                        Forgot Your Password?
                    </a>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="grid-x">
    <div class="o-banner">
        <Adsense
            data-ad-client="ca-pub-4370802149187412"
            data-ad-slot="7685160351">
        </Adsense>
    </div>
</div>
@endsection
