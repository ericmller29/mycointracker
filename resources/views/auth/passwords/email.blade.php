@extends('layouts.app')

@section('content')
<div class="grid-x grid-margin-y">
    <div class="cell">
        <div class="o-card">
            <header class="o-card__header">
                <div class="grid-x align-middle">
                    <div class="cell small-12 medium-6">
                        <h2>reset your password</h2>
                    </div>
                </div>
            </header>
            <div class="o-card__content">
                <form method="POST" action="{{ route('password.email') }}">
                    <div class="cell small-12">
                        {{ csrf_field() }}
                        <div class="m-formItem">
                            @if ($errors->has('email'))
                                <span class="label alert">{{ $errors->first('email') }}</span>
                            @endif
                            <input 
                                type="text" 
                                name="email"
                                placeholder="Email:"
                                autocomplete="off">
                        </div>
                    </div>
                    <div class="cell small-12 text-center align-center" style="margin-top: 20px;">
                        <button 
                            class="button">send password reset link</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
