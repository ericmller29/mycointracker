@extends('layouts.app')

@section('content')
<div class="grid-x grid-margin-y">
    <div class="cell">
        <div class="o-card">
            <header class="o-card__header">
                <div class="grid-x align-middle">
                    <div class="cell small-12 medium-6">
                        <h2>reset your password</h2>
                    </div>
                </div>
            </header>
            <div class="o-card__content">
                <form method="POST" action="{{ route('password.email') }}">
                    <div class="cell small-12">
                        {{ csrf_field() }}
                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="m-formItem">
                            @if ($errors->has('password'))
                                <span class="label alert">{{ $errors->first('password') }}</span>
                            @endif
                            <input type="password" name="password" placeholder="New Password" required>
                        </div>
                        <div class="m-formItem">
                            @if ($errors->has('password_confirmation'))
                                <span class="label alert">{{ $errors->first('password_confirmation') }}</span>
                            @endif
                            <input type="password" name="password_confirmation" placeholder="Confirm New Password" required>
                        </div>
                    </div>
                    <div class="cell small-12 text-center align-center" style="margin-top: 20px;">
                        <button 
                            class="button">reset password</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
