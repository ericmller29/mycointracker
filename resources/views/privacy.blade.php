@extends('layouts.app')

@section('content')
<div class="grid-x text-center" v-if="showCoins" v-cloak>
    <div class="m-portfolioWorth">
    	<portfolio-worth></portfolio-worth>
    </div>
</div>
<div class="grid-x grid-margin-y">
    <div class="cell o-level">
        <div class="o-card">
	        <header class="o-card__header">
	            <div class="grid-x align-middle">
	                <div class="cell small-12 medium-6">
	                    <h2 class="o-card__title">privacy policy</h2>
	                </div>
	            </div>
	        </header>
	        <div class="o-card__content">
				<p>We take our users' privacy very seriously. This privacy policy has been compiled to better serve our users who are concerned with how their 'Personally Identifiable Information' (PII) is being used online. PII, as described in US privacy law and information security, is information that can be used on its own or with other information to identify, contact, or locate a single person, or to identify an individual in context. Please read our privacy policy carefully to get a clear understanding of how we collect, use, protect or otherwise handle your Personally Identifiable Information in accordance with our website.</p>

				<h2>What personal information do we collect from the people that visit our blog, website or app?</h2>
				<p>When ordering or registering on our site, as appropriate, you may be asked to enter your name, email address, cryptocurrency assets and holdings, or other details to help you with your experience.</p>

				<h2>When do we collect information?</h2>
				<p>We collect information from you when you register on our site, add or remove cryptocurrency holdings (coins) to your page, or enter information on our site.</p>

				<h2>How do we use your information?</h2>
				<p>We may use the information we collect from you when you register, add holdings to your list, respond to a survey or marketing communication, surf the website, or use certain other site features in the following ways:</p>
				<ul>
					<li>To personalize your experience and to allow us to deliver the type of content and product offerings in which you are most interested.</li>
					<li>To improve our website in order to better serve you.</li>
					<li>To allow us to better service you in responding to your customer service requests.</li>
					<li>To send periodic emails regarding our products and services (updates, alerts, etc.)</li>
				</ul>

				<h2>How do we protect your information?</h2>
				<p>We do not use vulnerability scanning and/or scanning to PCI standards. We only provide articles and information. We never ask for credit card numbers. We do not use Malware Scanning.</p>
				<p>Your personal information is contained behind secured networks and is only accessible by a limited number of persons who have special access rights to such systems, and are required to keep the information confidential. In addition, all information you supply is encrypted via Secure Socket Layer (SSL) technology.</p>
				<p>We implement a variety of security measures when a user enters, submits, or accesses their information to maintain the safety of your personal information.</p>

				<h2>Do we use 'cookies'?</h2>
				<p>Yes. Cookies are small files that a site or its service provider transfers to your computer's hard drive through your Web browser (if you allow) that enables the site's or service provider's systems to recognize your browser and capture and remember certain information. For instance, we use cookies to help us remember the coins you’ve added to your portfolio and how many of each coin you’ve added. They are also used to help us understand your preferences based on previous or current site activity, which enables us to provide you with improved services. We also use cookies to help us compile aggregate data about site traffic and site interaction so that we can offer better site experiences and tools in the future.</p>

				<h2>We use cookies to:</h2>
				<ul>
					<li>Understand and save user's preferences for future visits.</li>
					<li>Compile aggregate data about site traffic and site interactions in order to offer better site experiences and tools in the future. We may also use trusted third-party services that track this information on our behalf.</li>
				</ul>
				<p>You can choose to have your computer warn you each time a cookie is being sent, or you can choose to turn off all cookies. You do this through your browser settings. Since browser is a little different, look at your browser's Help Menu to learn the correct way to modify your cookies.</p>

				<h2>If users disable cookies in their browser:</h2>
				<p>If you turn cookies off, some of the features that make your site experience more efficient may not function properly.</p>

				<h2>Third-party disclosure</h2>
				<p>We do not sell, trade, or otherwise transfer to outside parties your Personally Identifiable Information. This does not include website hosting partners and other parties who assist us in operating our website, conducting our business, or serving our users, so long as those parties agree to keep this information confidential. We may also release information when it's release is appropriate to comply with the law, enforce our site policies, or protect ours or others' rights, property or safety.</p>
				<p>However, non-personally identifiable visitor information may be provided to other parties for marketing, advertising, or other uses.</p>

				<h2>Third-party links</h2>
				<p>We do not include or offer third-party products or services on our website.</p>

				<h2>Google</h2>
				<p>Google's advertising requirements can be summed up by Google's Advertising Principles. They are put in place to provide a positive experience for users. <a href="https://support.google.com/adwordspolicy/answer/1316548?hl=en" target="_blank">https://support.google.com/adwordspolicy/answer/1316548?hl=en</a></p>
				<p>We use Google AdSense Advertising on our website.</p>
				<p>Google, as a third-party vendor, uses cookies to serve ads on our site. Google's use of the DART cookie enables it to serve ads to our users based on previous visits to our site and other sites on the Internet. Users may opt-out of the use of the DART cookie by visiting the Google Ad and Content Network privacy policy.</p>

				<h2>We have implemented the following:</h2>
				<ul>
					<li>Demographics and Interests Reporting</li>
				</ul>
				<p>We, along with third-party vendors such as Google use first-party cookies (such as the Google Analytics cookies) and third-party cookies (such as the DoubleClick cookie) or other third-party identifiers together to compile data regarding user interactions with ad impressions and other ad service functions as they relate to our website.</p>

				<h2>Opting out:</h2>
				<p>Users can set preferences for how Google advertises to you using the Google Ad Settings page. Alternatively, you can opt out by visiting the Network Advertising Initiative Opt Out page or by using the Google Analytics Opt Out Browser add on.</p>

				<h2>California Online Privacy Protection Act</h2>
				<p>CalOPPA is the first state law in the nation to require commercial websites and online services to post a privacy policy. The law's reach stretches well beyond California to require any person or company in the United States (and conceivably the world) that operates websites collecting Personally Identifiable Information from California consumers to post a conspicuous privacy policy on its website stating exactly the information being collected and those individuals or companies with whom it is being shared. - See more at: <a href="http://consumercal.org/california-online-privacy-protection-act-caloppa/#sthash.0FdRbT51.dpuf" target="_blank">http://consumercal.org/california-online-privacy-protection-act-caloppa/#sthash.0FdRbT51.dpuf</a></p>

				<h2>According to CalOPPA, we agree to the following:</h2>
				<p>Users can visit our site anonymously.</p>
				<p>Once this privacy policy is created, we will add a link to it on our home page or as a minimum, on the first significant page after entering our website.</p>
				<p>Our Privacy Policy link includes the word 'Privacy' and can easily be found on the page specified above.</p>
				<p>You will be notified of any Privacy Policy changes:</p>
				<ul>
					<li>On our Privacy Policy Page</li>
				</ul>
				<p>Can change your personal information:</p>
				<ul>
					<li>By logging in to your account</li>
				</ul>
				<h2>How does our site handle Do Not Track signals?</h2>
				<p>We don't honor Do Not Track signals and Do Not Track, plant cookies, or use advertising when a Do Not Track (DNT) browser mechanism is in place. With DNT signals, we are unable to store and display coins that a user has added to their online portfolio.</p>

				<h2>Does our site allow third-party behavioral tracking?</h2>
				<p>It's also important to note that we do not allow third-party behavioral tracking</p>
			</div>
        </div>
    </div>
</div>
@endsection