@extends('layouts.app')

@section('content')
<div class="grid-x text-center" v-if="showCoins" v-cloak>
    <portfolio-worth></portfolio-worth>
</div>
<div class="grid-x grid-margin-y">
    <div class="cell">
        <div class="o-card">
        <header class="o-card__header">
            <div class="grid-x align-middle">
                <div class="cell small-12 medium-6">
                    <h2>
                        my holdings
                        <span v-if="refreshing">@svg('loader')</span>
                    </h2>
                </div>
                <div class="cell small-12 medium-6 text-right">
                    <div class="grid-x align-middle align-right">
                        <div class="cell small-6">
                            <div class="m-selectbox">
                                <select 
                                    v-model="sort"
                                    v-on:change="setSort()">
                                    <option :value="null">sort by:</option>
                                    <option value="amount"># of coins</option>
                                    <option value="purse_amount">$ amount</option>
                                    <option value="change_1">1 hour change</option>
                                    <option value="change_24">24 hour change</option>
                                    <option value="change_7_days">7 day change</option>
                                </select>
                                @svg('chevron-down')
                            </div>
                        </div>
                        <div class="cell small-6 medium-1">
                            <a href="#" v-on:click.prevent="showModal = true" class="a-addButton">
                                @svg('squared-plus')
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div class="o-coins" v-if="showCoins" v-cloak>
            <div class="m-coin" v-if="coinList.length === 0">
                <div class="grid-x align-middle">
                    <div class="cell small-12">
                        <span class="m-coin__price__noCoins">
                            <a href="#" v-on:click.prevent="showModal = true">Add a coin</a> to get started or <a href="{{ route('login') }}">login</a> to view your portfolio!
                        </span>
                    </div>
                </div>
            </div>
            <div class="m-coin" v-for="coin in coinList">
                <div class="grid-x align-middle">
                    <div class="cell small-6">
                        <div class="grid-x align-middle">
                            <a href="#" v-on:click.prevent="deleteCoin(coin.id)" class="m-coin__delete">
                                @svg('squared-cross')
                            </a>
                            <a href="#" v-on:click.prevent="edit(coin)" class="m-coin__total">@{{ coin.coin }} @{{ coin.amount }}</a>
                            <span class="m-coin__sep show-for-medium"></span>
                            <span class="m-coin__price">@{{ coin.current_price | currency }}</span>
                        </div>
                    </div>
                    <div class="cell small-6">
                        <div class="grid-x align-middle align-right">
                            <span class="m-coin__price show-for-medium"
                                v-bind:class="{
                                    '-good': coin.change_24 > 0,
                                    '-bad': coin.change_24 < 0}">@{{ ((coin.change_24 > 0) ? '+' : '') + coin.change_24 }}%</span>
                            <span class="m-coin__sep show-for-medium"></span>
                            <span class="m-coin__total">@{{ coin.purse_amount | currency }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>
<div class="grid-x">
    <div class="o-banner">
        <Adsense
            data-ad-client="ca-pub-4370802149187412"
            data-ad-slot="5076440124">
        </Adsense>
    </div>
</div>
<div class="grid-x grid-margin-x grid-margin-y align-stretch" v-if="coinList.length > 0">
    <div class="small-12 medium-6 cell">
        <div class="o-card">
            <header class="o-card__header">
                <div class="grid-x align-middle">
                    <div class="cell small-6">
                        <h2>distribution</h2>
                    </div>
                </div>
            </header>
            <div class="o-card__content">
                <div class="grid-x">
                    <div class="cell small-12">
                        <pie-chart></pie-chart>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="small-12 medium-6 cell">
        <div class="o-card">
            <header class="o-card__header">
                <div class="grid-x align-middle">
                    <div class="cell small-6">
                        <h2>gains / losses</h2>
                    </div>
                    <div class="cell small-6 text-right">
                        <div class="grid-x align-middle align-right">
                            <div class="m-selectbox">
                                <select v-model="priceChange" v-on:change="changePrice()">
                                    <option value="1h">1h Change</option>
                                    <option value="24h">24h Change</option>
                                    <option value="7d">7d Change</option>
                                </select>
                                @svg('chevron-down')
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <div class="o-card__content">
                <div class="grid-x">
                    <div class="cell small-12">
                        <stacked-chart></stacked-chart>
<!--                         <chartjs-bar
                            :bind="true"
                            :option="stackedGraph.options"
                            :datasets="stackedGraph.datasets"
                            :labels="stackedGraph.labels"></chartjs-bar> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="grid-x" v-if="coinList.length > 0">
    <div class="o-banner">
        <Adsense
            data-ad-client="ca-pub-4370802149187412"
            data-ad-slot="5076440124">
        </Adsense>
    </div>
</div>
<div class="grid-x grid-margin-x grid-margin-y align-stretch">
    <div class="small-12 medium-6 cell">
        <div class="o-card m-news">
            <header class="o-card__header">
                <div class="grid-x align-middle">
                    <div class="cell small-6">
                        <h2>crypto news</h2>
                    </div>
                </div>
            </header>
            <div class="o-card__content">
                @foreach($articles as $a)
                <article class="m-article">
                    <h2 class="m-article__title"><a href="{{ $a->url }}" alt="{{ $a->title }}" target="_blank">{{ $a->title }}</a></h2>
                    <time datetime="{{ $a->published }}">@svg('calendar') {{ $a->published }}</time>
                </article>
                @endforeach
            </div>
        </div>
    </div>
    <div class="small-12 medium-6 cell">
        <div class="o-card m-twitter">
            <header class="o-card__header">
                <div class="grid-x align-middle">
                    <div class="cell small-6">
                        <h2>twitter feed</h2>
                    </div>
                </div>
            </header>
            <div class="o-card__content">
                <a class="twitter-timeline" data-lang="en" data-height="100%" href="https://twitter.com/mycointracker?ref_src=twsrc%5Etfw" data-tweet-limit="3" data-chrome="nofooter noheader transparent noborders">Tweets by mycointracker</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
            </div>
        </div>
    </div>
</div>
<modal v-if="showModal" @close="showModal = false">
    <h2 slot="title">add a coin</h2>
    <div slot="content">
        <add-coin></add-coin>
    </div>
</modal>
<modal v-if="showEditModal" @close="showEditModal = false">
    <h2 slot="title">edit your @{{ editCoin.coin }}</h2>
    <div slot="content">
        <edit-coin :coin="editCoin"></edit-coin>
    </div>
</modal>
@endsection