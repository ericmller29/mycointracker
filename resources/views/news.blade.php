@extends('layouts.app')

@section('content')
<div class="grid-x text-center" v-if="showCoins" v-cloak>
    <div class="m-portfolioWorth">
		<portfolio-worth></portfolio-worth>
    </div>
</div>
<div class="grid-x grid-margin-y">
    <div class="cell">
        <div class="o-card">
	        <header class="o-card__header">
	            <div class="grid-x align-middle">
	                <div class="cell small-12 medium-6">
	                    <h2>latest crypto news</h2>
	                </div>
	            </div>
	        </header>
	        <div class="o-card__content">
	        	@foreach($articles as $a)
	        		<article class="m-article">
	        			<h2 class="m-article__title"><a href="{{ $a->url }}" alt="{{ $a->title }}" target="_blank">{{ $a->title }}</a></h2>
	        			<time datetime="{{ $a->published }}">@svg('calendar') {{ $a->published }}</time>
	        			<div class="m-article__content">
	        				<p>{{ $a->snippet }}</p>
	        			</div>
	        			<div class="m-article__buttons">
							<a href="{{ $a->url }}" alt="{{ $a->title }}" class="button" target="_blank">read more</a>
	        			</div>
	        		</article>
	        	@endforeach
	        </div>
    	</div>
	</div>
</div>
<!-- <div class="grid-x">
    <div class="o-banner">
        <Adsense
            data-ad-client="ca-pub-4370802149187412"
            data-ad-slot="6650725374">
        </Adsense>
    </div>
</div> -->
<div class="grid-x grid-margin-y">
	<div class="cell">
		<div class="o-card">
			<div class="grid-x align-middle">
				<div class="cell small-6">
					<span><strong>pagination:</strong></span>
				</div>
				<div class="cell small-6">
					{{ $articles->links() }}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection