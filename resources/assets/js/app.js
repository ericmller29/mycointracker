
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import VueNoty from 'vuejs-noty';
import Vue2Filters from 'vue2-filters'
import Ads from 'vue-google-adsense'

require('./bootstrap');

window.Vue = require('vue');
 
Vue.use(require('vue-script2'))
Vue.use(Ads.Adsense);

Vue.use(Vue2Filters)
Vue.use(VueNoty, {
	timeout: 2000,
	progressBar: false,
	layout: 'bottomRight'
});

window.coinEmit = new Vue();
window.appEmit = new Vue();

Vue.component('add-coin', require('./components/AddCoinComponent.vue'));
Vue.component('edit-coin', require('./components/EditCoinComponent.vue'));
Vue.component('coin-search', require('./components/CoinSearch.vue'));
Vue.component('modal', require('./components/Modal.vue'));
Vue.component('pie-chart', require('./charts/PieChart.vue'));
Vue.component('stacked-chart', require('./charts/StackedChart.vue'));
Vue.component('portfolio-worth', require('./components/PortfolioWorth.vue'));

const app = new Vue({
    el: '#app',
    data: {
    	showModal: false,
        showEditModal: false,
        refreshing: false,
        coinList: [],
        showCoins: false,
        purseAmount: 0,
        sort: null,
        editCoin: null,
        priceChange: '24h',
        showUsd: true
    },
    mounted: function(){
    	var _this = this;
        
    	appEmit.$on('modalToggle', function(e){
    		_this.showModal = e;
    	});

        appEmit.$on('editModalToggle', function(e){
            _this.showEditModal = e;
        });

        coinEmit.$on('coinsUpdated', this.updateValues);

        axios.get('/api/coins')
            .then(_this.updateValues);

        setInterval(function(){
            _this.refreshing = true;
            axios.get('/api/coins')
                .then(_this.updateValues);
        }, 60000);
    },
    methods: {
        changePrice(){
            var _this = this;
                
            appEmit.$emit('valueChange', _this.priceChange);
        },
        deleteCoin: function(id){
            var _this = this;

            axios.post('/api/coins/delete/' + id, {})
                .then(_this.updateValues)
        },
        updateValues(res){
            var _this = this;
            _this.refreshing = false;


            _this.coinList = res.data.coins;
            _this.purseAmount = res.data.purse;
            _this.sort = res.data.sorted;
            _this.showCoins = true;

            document.title = '$' + _this.purseAmount.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,') + ' | mycointracker.io';

            if(_this.coinList.length > 0){
                appEmit.$emit('valueChange', _this.priceChange);
            }
        },
        setSort(){
            axios.get('/api/coins?sort=' + this.sort)
                .then(this.updateValues);
        },
        edit(coin){
            this.showEditModal = true;
            this.editCoin = coin;
        }
    }
});
