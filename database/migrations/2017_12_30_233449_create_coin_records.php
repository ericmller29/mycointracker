<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoinRecords extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coin_records', function (Blueprint $table) {
            $table->increments('id');
            $table->string('coin');
            $table->integer('amount');
            $table->integer('token_id')->unsigned();
            $table->timestamps();

            $table->foreign('token_id')->references('id')->on('user_tokens');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coin_records');
    }
}
