<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['token'])->group(function(){
	Route::get('/', 'HomeController@index')->name('home');

	Auth::routes();

	Route::post('/register', 'Auth\RegisterController@create')->name('register');
	Route::get('/register', function(){
		return view('auth.register');
	});

	Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout')->name('logout');

	Route::prefix('api')->group(function(){
		Route::get('/purse', 'CoinController@purseAmount');

		Route::get('/marketdata', 'MarketdataController@get');
		Route::get('/marketdata/find', 'MarketdataController@find');
		Route::get('/marketdata/top50', 'MarketdataController@top50');

		Route::get('/coins', 'CoinController@getCoins');
		Route::post('/coins/create', 'CoinController@create');
		Route::post('/coins/delete/{id}', 'CoinController@delete');
		Route::post('/coins/edit/{id}', 'CoinController@save');

		Route::get('/graphs/pie', 'GraphController@pie');
		Route::get('/graphs/stacked', 'GraphController@stacked');
	});

	Route::get('/news', 'NewsController@index')->name('news');
	Route::get('/privacy-policy', function(){
		return view('privacy');
	})->name('privacy');
	Route::get('/terms-of-service', function(){
		return view('terms');
	})->name('terms');
});