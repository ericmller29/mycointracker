<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class News extends Model
{
    protected $table = 'news';
    protected $fillable = ['title', 'snippet', 'published', 'url', 'image'];

    public function getPublishedAttribute($val){
    	return Carbon::parse($val)->setTimezone('America/New_York')->format('F d, Y h:ia');
    }
}
