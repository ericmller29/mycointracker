<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tokens extends Model
{
    
    protected $table = 'user_tokens';
    protected $fillable = ['token'];

    public function coins(){
    	return $this->hasMany('App\Coins', 'token_id', 'id');
    }

    public function user(){
    	return $this->hasOne('App\User', 'token_id', 'id');
    }
}
