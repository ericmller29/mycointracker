<?php

namespace App\Http\Middleware;

use Closure;

use Cookie;

use App\Tokens;

use Auth;

class TokenCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()){
            $token = Auth::user()->token()->first();

            $request->attributes->add(['token' => $token]);

            return $next($request);
        }
        
        if(!isset($_COOKIE['token'])){
            $token = $this->checkToken();

            setcookie('token', $token, time()+31556926);
            $_COOKIE['token'] = $token;
        }
            
        $request->attributes->add(['token' => Tokens::where('token', $_COOKIE['token'])->first()]);

        return $next($request);
    }

    public function checkToken(){
        $generated = md5(uniqid(rand(), true));
        $token = Tokens::where('token', $generated)->first();

        if(isset($token)){
            $this->checkToken();
        }

        $token = new Tokens();
        $token->token = $generated;
        $token->save();

        return $generated;
    }
}
