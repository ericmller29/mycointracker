<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Marketdata;

class MarketdataController extends Controller
{
    public function get(Request $request){
    	$data = $this->getIcon(Marketdata::where('symbol', $request->get('abbr'))->first());

    	return response()->json($data);
    }

    public function find(Request $request){

        if(!empty($request->get('abbr'))){
            $data = collect(Marketdata::where('symbol', 'like', '%' . $request->get('abbr') . '%')->orWhere('name', 'like', '%' . $request->get('abbr') . '%')->get());
        }else{
            $data = collect(Marketdata::orderBy('rank')->limit(50)->get());
        }
        $dataWithIcons = $data->map(function($data){ return $this->getIcon($data); });

        return response()->json($dataWithIcons);
    }

    public function top50(Request $request){
    	$data = collect(Marketdata::orderBy('rank')->limit(50)->get());

        $dataWithIcons = $data->map(function($data){
            return $this->getIcon($data);
        });

        // die($request->get('token'));
    	return response()->json($dataWithIcons);
    }

    private function getIcon($data){
        $icon = file_exists(public_path() . '/img/svg/coins/' . $data->symbol . '.svg');
        $data['icon'] = file_get_contents(public_path() . '/img/svg/coins/default.svg');

        if($icon){
            $data['icon'] = file_get_contents(public_path() . '/img/svg/coins/' . $data->symbol . '.svg');
        }

        return $data;
    }
}
