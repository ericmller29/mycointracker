<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Marketdata;

class GraphController extends Controller
{
    public function pie(Request $request){
    	$token = $request->get('token');
    	$coins = collect($token->coins()->get());

    	$sum = $coins->sum(function($coin){
            $actual_coin = Marketdata::where('symbol', '=', $coin->coin)->first();

    		return $coin->amount * $actual_coin->price_usd;
    	});

    	$pie['labels'] = $coins->map(function($coin){
    		return $coin->coin;
    	});

    	$pie['values'] = $coins->map(function($coin) use ($sum){
    		// return $coin->amount;
            $actual_coin = Marketdata::where('symbol', '=', $coin->coin)->first();

    		// return round(($coin->amount * $actual_coin->price_usd) / ($sum / 100));
            return round($coin->amount * $actual_coin->price_usd, 2);
    	});

    	return response()->json($pie);
    }
    public function stacked(Request $request){
        $token = $request->get('token');
        $coins = collect($token->coins()->get());
        $changeData = [
            new CoinData('Previous Value'), 
            new CoinData('Gain'), 
            new CoinData('Current Value'),
            new CoinData('Loss')];

        $colors = ['good' => 'rgba(85,233,188,0.5)', 'bad' => 'rgba(247,56,89,0.5)', 'primary' => 'rgba(24,145,172,0.8)', 'light_primary' => 'rgba(24,145,172,0.5)'];

        $percent = $request->get('change');

        $stacked['labels'] = $coins->map(function($coin){
            return $coin->coin;
        });

        $coins->map(function($coin) use ($changeData, $colors, $percent){
            $actual_coin = Marketdata::where('symbol', '=', $coin->coin)->first();

            $current_price = $actual_coin->price_usd;
            $your_amount = round($coin->amount * $actual_coin->price_usd, 2);

            if(!isset($percent) || $percent == '24h'){
                $percent_change = $actual_coin->percent_change_24h;
            }else if($percent == '1h'){
                $percent_change = $actual_coin->percent_change_1h;
            }else if($percent == '7d'){
                $percent_change = $actual_coin->percent_change_7d;
            }

            $yesterday = round($your_amount / (1 + ($percent_change / 100)), 2);
            $difference = round($your_amount - $yesterday, 2);

            // return $data;
            array_push($changeData[0]->backgroundColor, $colors['primary']);
            array_push($changeData[0]->data, ($difference < 0) ? null : $yesterday);

            array_push($changeData[1]->backgroundColor, $colors['good']);
            array_push($changeData[1]->data, ($difference < 0) ? null : $difference);

            array_push($changeData[2]->backgroundColor, $colors['light_primary']);
            array_push($changeData[2]->data, ($difference > 0) ? null : $your_amount);

            array_push($changeData[3]->backgroundColor, $colors['bad']);
            array_push($changeData[3]->data, ($difference > 0) ? null : $difference);
        });

        return response()->json(['labels' => $stacked['labels'], 'datasets' => $changeData]);
    }
}

class CoinData {
    public $label;
    public $backgroundColor = [];
    public $data = [];

    function __construct($label, $backgroundColor = [], $data = []){
        $this->label = $label;
        $this->backgroundColor = $backgroundColor;
        $this->data = $data;
    }
}
