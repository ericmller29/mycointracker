<?php

namespace App\Http\Controllers;

use Validator;

use Illuminate\Http\Request;

use App\Coins as Coin;
use App\Tokens;
use App\Marketdata;
use Illuminate\Cookie\CookieJar;

class CoinController extends Controller
{
    public function purseAmount(Request $request){
        $token = $request->get('token');
        $coins = collect($token->coins()->get());

        $usd = $coins->sum(function($coin){
            $actualCoin = Marketdata::where('symbol', '=', $coin->coin)->first();

            return $coin->amount * $actualCoin->price_usd;
        }); 

        $btc = $coins->sum(function($coin){
            $actualCoin = Marketdata::where('symbol', '=', $coin->coin)->first();

            return $coin->amount * $actualCoin->price_btc;
        }); 

        return response()->json(['usd' => round($usd,2), 'btc' => round($btc,8)], 200);
    }
	public function getCoins(CookieJar $cookieJar, Request $request){
		$token = $request->get('token');

        if($request->get('sort')){
            $sort = $request->get('sort');

            setcookie('homepage_sort', $sort, time()+31556926);
            $_COOKIE['homepage_sort'] = $sort;
        }else if(!isset($_COOKIE['homepage_sort'])){
            $_COOKIE['homepage_sort'] = null;
        }

        return response()->json(['coins' => $this->returnCoins($token, $_COOKIE['homepage_sort']), 'purse' => $this->getPurse($token), 'sorted' => $_COOKIE['homepage_sort']], 200);
		// return response()->json($this->getPurse($token));
	}
    public function create(Request $request){
    	$token = $request->get('token');

    	$validate = $request->validate([
    		'amount' => 'required',
    		'coin' => 'required'
    	]);

    	$usercoin = $token->coins()->where('coin', $request->coin)->first();

    	if(isset($usercoin)){
    		return response()->json(['message' => 'You\'ve already added ' . $request->get('coin') . ' to your portfolio.'], 400);
    	}

    	$coin = new Coin();
    	$coin->coin = $request->get('coin');
    	$coin->amount = $request->get('amount');
    	$token->coins()->save($coin);
        
        // $actualCoin = Marketdata::where('symbol', $coin->coin)->first();

        // $coin['current_price'] = $actualCoin->price_usd;
        // $coin['purse_amount'] = $actualCoin->price_usd * $coin->amount;
        // $coin['change_24'] = $actualCoin->percent_change_24h;

    	// return response()->json($this->returnCoins($token), 200);
        return response()->json(['coins' => $this->returnCoins($token), 'purse' => $this->getPurse($token)], 200);
    }
    public function delete(Request $request, $id){
        $token = $request->get('token');
        $coin = $token->coins()->find($id);
        $coin->delete();

        // return response()->json($this->returnCoins($token), 200);
        return response()->json(['coins' => $this->returnCoins($token), 'purse' => $this->getPurse($token)], 200);
    }

    public function save(Request $request, $id){
        $token = $request->get('token');
        $coin = $token->coins()->find($id);

        $validate = $request->validate([
            'amount' => 'required'
        ]);

        if(!isset($coin)){
            return response()->json(['message' => 'You do not have this coin in your wallet.'], 400);
        }

        $coin->amount = $request->get('amount');
        $coin->save();

        return response()->json(['coins' => $this->returnCoins($token), 'purse' => $this->getPurse($token)], 200);
    }

    private function returnCoins($token, $sort = null){
        $coins = collect($token->coins()->get());

        $coinsParse = $coins->map(function($coin){
            $actualCoin = Marketdata::where('symbol', $coin->coin)->first();

            $coin['current_price'] = $actualCoin->price_usd;
            $coin['purse_amount'] = $actualCoin->price_usd * $coin->amount;
            $coin['change_24'] = $actualCoin->percent_change_24h;
            $coin['change_1'] = $actualCoin->percent_change_1h;
            $coin['change_7_days'] = $actualCoin->percent_change_7d;

            return $coin;
        });

        $sorted = $coinsParse->sortByDesc($sort);

        return $sorted->values()->all();
    }

    private function getPurse($token){
        $coins = collect($token->coins()->get());
        $purse = 0;

        $purseAmount = $coins->sum(function($coin){
            $actualCoin = Marketdata::where('symbol', '=', $coin->coin)->first();

            return $coin->amount * $actualCoin->price_usd;
        }); 

        return $purseAmount;
    }
}
