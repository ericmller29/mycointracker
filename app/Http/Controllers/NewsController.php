<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;

class NewsController extends Controller
{
    public function index(){
    	$data['articles'] = News::orderBy('published', 'desc')->paginate(5);

    	return view('news', $data);
    }
}
