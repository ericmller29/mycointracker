<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coins extends Model
{
    protected $table = 'coin_records';
    protected $fillable = ['coin', 'amount', 'token_id'];

    protected $casts = [
    	'amount' => 'float'
    ];
}
