<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\News;

use Carbon\Carbon;

class GetNews extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'news:gather';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get the latest crypto news.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $pruneToDay = Carbon::now()->subDays(2);

        $client = new \GuzzleHttp\Client();
        $request = $client->request('GET', 'https://newsapi.org/v2/everything?sources=crypto-coins-news&apiKey=' . env('NEWS_API_KEY'));
        //1 request every hour please
        $body = json_decode($request->getBody());
        $count = 0;

        $prune_these = News::where('created_at', '<', $pruneToDay);
        $prune_count = $prune_these->get()->count();

        $prune_these->delete();
        $this->info($prune_count . ' articles have been deleted');

        foreach($body->articles as $article){
            $foundArticle = News::where('title', $article->title)->first();

            if(isset($foundArticle)){
                $this->info('No new items.');
                return false;
            }

            $newsItem = new News();
            $newsItem->title = $article->title;
            $newsItem->snippet = $article->description;
            $newsItem->published = $article->publishedAt;
            $newsItem->url = $article->url;
            $newsItem->image = $article->urlToImage;
            $newsItem->save();

            $count = $count + 1;
        }
        $this->info($count . ' new articles added.');
    }
}
