<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Marketdata;

class Tweeter extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tweet:prices';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Tweet the prices of coins.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data = Marketdata::orderBy('rank')->limit(10)->get();
        $collection = collect($data);
        $coin = $collection->random();
        $clean_coin_name = strtolower($coin->name);
        $clean_coin_name = preg_replace("/[^a-z0-9_\s-]/", "", $clean_coin_name);
        $clean_coin_name = preg_replace("/[\s-]+/", " ", $clean_coin_name);
        $clean_coin_name = preg_replace("/[\s_]/", " ", $clean_coin_name);
        $url = app('bitly')->getUrl('https://coinmarketcap.com/currencies/' . $clean_coin_name);

        $tweet = $coin->symbol . ' has changed by ' . $coin->percent_change_1h . '% in the last hour! Find out more here ' . $url . '! Don\'t forget you can track all your coins at MyCoinTracker.io!';

        $this->info($tweet);
    }
}
